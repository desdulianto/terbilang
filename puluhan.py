def terbilang(angka):
    # satuan
    if angka < 10:
        return satuan[angka]
    # puluhan
    elif angka >= 10 and angka <= 90:
        awalan = satuan[angka/10]
        akhiran = ''
        if angka % 10 > 0:
            akhiran = terbilang(angka % 10)
        return awalan + ' puluh ' + akhiran
