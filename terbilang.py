satuan = ['nol', 'satu', 'dua', 'tiga', 'empat',
          'lima', 'enam', 'tujuh', 'delapan', 'sembilan']

ket = {10: 'puluh', 100: 'ratus', 1000: 'ribu', 1000000: 'juta',
          1000000000: 'milyar', 1000000000000: 'triliun'}

keys = ket.keys()
keys.sort(reverse=True)

def terbilang(angka):
    # satuan
    if angka < 10:
        return satuan[angka]
    # belasan
    elif angka >= 11 and angka <= 19:
        awalan = satuan[angka%10]
        if awalan == 'satu': awalan = 'se'
        return '%s%s%s' % (awalan,
                           ' ' if awalan != 'se' else '',
                           'belas')
    # dan selebihnya
    else:
        for i in keys:
            if angka < i:
                continue
            awalan = satuan[angka/i] if angka / i < 10 else terbilang(angka/i)
            if awalan == 'satu' and angka < 2000:
                awalan = 'se'

            # puluh, ratus, ribu dst
            tengah = ket[i]

            akhiran = ''
            if angka % i > 0:
                akhiran = terbilang(angka % i)            
            return '%s%s%s%s%s' % (awalan,
                                 ' ' if awalan != 'se' else '',
                                 tengah,
                                 ' ' if akhiran != '' else '',
                                 akhiran)

if __name__ == '__main__':
    test = [0, 1, 3, 7, 10, 11, 13, 27, 30, 39, 50, 70, 75, 99, 109,
            213, 353, 1735, 1001222, 3423532, 27983125, 934569023, 
            1750287000, 10389000000, 893756233123, 123578320126389]
    for i in test:
        print terbilang(i)


